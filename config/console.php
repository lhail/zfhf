<?php
// +----------------------------------------------------------------------
// | 控制台配置
// +----------------------------------------------------------------------
return [
    // 指令定义
    'commands' => [
        'sync' => \app\api\command\v1\Sync::class,
        'clearUser' => \app\api\command\v1\ClearUser::class,
        'facts' => \app\command\Facts::class,
    ],
];
