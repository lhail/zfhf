<?php


return [
    //政风行风小程序
    'zfhf' => [
        'app_id' => env('WECHAT.app_id', ''),
        'app_secret' => env('WECHAT.app_secret', ''),
        'grant_type' => env('WECHAT.grant_type', ''),
    ]
];
