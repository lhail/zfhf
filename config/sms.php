<?php

return [
    'AppId' => env('CODE.AppID',''),
    'AppKey' => env('CODE.KEY',''),
    'sign' => env('CODE.SIGN',''),
    'template' => env('CODE.TEMPLATE',''),
];
