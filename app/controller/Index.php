<?php
namespace app\controller;

use app\api\model\v1\Facts;
use app\api\model\v1\OnlineUnitDetail;
use app\BaseController;

class Index extends BaseController
{
    public function index()
    {
        $day = OnlineUnitDetail::doNextDay('2023-12-31',7);
        var_dump($day);

        (new Facts())->doPlan(42);

        return '你好';
    }

    public function hello($name = 'ThinkPHP6')
    {
        return 'hello,' . $name;
    }
}
