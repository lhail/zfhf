<?php
declare (strict_types = 1);

namespace app;

use app\api\helpers\v1\jwt_token;
use think\App;
use think\cache\driver\Redis;
use think\Collection;
use think\exception\ValidateException;
use think\facade\Config;
use think\Paginator;
use think\Validate;

/**
 * 控制器基础类
 */
abstract class BaseController
{
    /**
     * Request实例
     * @var \think\Request
     */
    protected $request;

    /**
     * 应用实例
     * @var \think\App
     */
    protected $app;

    /**
     * 是否批量验证
     * @var bool
     */
    protected $batchValidate = false;

    /**
     * 控制器中间件
     * @var array
     */
    protected $middleware = [];

    protected $token;

    protected $redis = null;
    /**
     * 构造方法
     * @access public
     * @param  App  $app  应用对象
     */
    public function __construct(App $app)
    {
        $this->app     = $app;
        $this->request = $this->app->request;
        $this->token = new jwt_token();
        $redis = Config::get('redis');
        $this->redis = new Redis($redis);
        // 控制器初始化
        $this->initialize();
    }

    // 初始化
    protected function initialize()
    {}

    /**
     * 返回状态说明:
     * 200请求成功
     * 304资源文件未做修改
     * 402用户未登录
     * 403资源文件请求被拒绝
     * 404请求的资源（网页等）不存在
     * 500业务处理逻辑错误
     * 501参数类型错误，不支持请求
     * 502服务端接收无效请求
     * @User: 刘海龙
     * @Date: 2021/7/22
     * @Time: 15:36
     * @return \think\response\Json
     *
     */
    public function response()
    {
        $args = func_get_args();
        $data = [
            'code' => 200,
            'data' => [
                'message' => '执行成功'
            ]
        ];
        foreach ($args as $arg) {
            switch ($arg) {
                case is_string($arg):
                    $data['data']['message'] = $arg;
                    break;
                case is_numeric($arg):
                    $data['code'] = $arg;
                    break;
                default:
                    if ($arg instanceof Collection || is_array($arg)) {
                        $data['data']['list'] = $arg;
                    } elseif ($arg instanceof Paginator) {
                        $data['data']['totalCount'] = $arg->total();
                        $data['data']['pageNo'] = $arg->currentPage();
                        $data['data']['recordCount'] = $arg->count();
                        $data['data']['pageSize'] = $arg->listRows();
                        $data['data']['totalPage'] = $arg->lastPage();
                        $data['data']['list'] = $arg->items();
                    } else {
                        $data['data']['content'] = $arg;
                    }
                    break;
            }
        }
        return json($data);
    }
    /**
     * 验证数据
     * @access protected
     * @param  array        $data     数据
     * @param  string|array $validate 验证器名或者验证规则数组
     * @param  array        $message  提示信息
     * @param  bool         $batch    是否批量验证
     * @return array|string|true
     * @throws ValidateException
     */
    protected function validate(array $data, $validate, array $message = [], bool $batch = false)
    {
        if (is_array($validate)) {
            $v = new Validate();
            $v->rule($validate);
        } else {
            if (strpos($validate, '.')) {
                // 支持场景
                [$validate, $scene] = explode('.', $validate);
            }
            $class = false !== strpos($validate, '\\') ? $validate : $this->app->parseClass('validate', $validate);
            $v     = new $class();
            if (!empty($scene)) {
                $v->scene($scene);
            }
        }

        $v->message($message);

        // 是否批量验证
        if ($batch || $this->batchValidate) {
            $v->batch(true);
        }

        return $v->failException(true)->check($data);
    }

    /**
     * @param $url
     * @param $method
     * @param null $postfields
     * @param array $headers
     * @User: 刘海龙
     * @Date: 2021/9/3
     * @Time: 10:22
     * @return array
     * http 请求
     */
    public function http($url, $method, $postfields = null, $headers = array()){
        $ci = curl_init();
        curl_setopt($ci, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ci, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ci, CURLOPT_TIMEOUT, 30);
        curl_setopt($ci, CURLOPT_RETURNTRANSFER, true);
        switch ($method) {
            case 'POST':
                curl_setopt($ci, CURLOPT_POST, true);
                if (!empty($postfields)) {
                    curl_setopt($ci, CURLOPT_POSTFIELDS, $postfields);
                    $this->postdata = $postfields;
                }
                break;
        }
        curl_setopt($ci, CURLOPT_URL, $url);
        curl_setopt($ci, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ci, CURLINFO_HEADER_OUT, true);

        $response = curl_exec($ci);
        $http_code = curl_getinfo($ci, CURLINFO_HTTP_CODE);

        curl_close($ci);
        return array($http_code, $response);
    }

}
