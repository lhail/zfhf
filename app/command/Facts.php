<?php
declare (strict_types=1);

namespace app\command;

use app\api\controller\v1\Message;
use app\api\model\v1\Admins;
use app\api\model\v1\OnlineUnitDetail;
use app\api\model\v1\Users;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

class Facts extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('facts')
            ->setDescription('the facts command');
    }

    protected function execute(Input $input, Output $output)
    {
        set_time_limit(0);
        $day = date("Y-m-d");
        $online = OnlineUnitDetail::where("online_date", $day)->find();
        if ($online && $online->online_status < 3) {
            $hour = intval(date("H"));
            $qty = \app\api\model\v1\Facts::whereNull("reply_date")->where("plan_date", "<=", $day)->count();
            if ($qty) {
                //手机号
                $mobiles = ['18105645858', '19982755204'];
//                    $mobiles=['18602591982'];
                foreach ($mobiles as $mobile) {
                    //短信回复
                    Message::sendMessage($mobile, "", 1, $qty."个待回复", 0);
                }


            }

            //同步发送联络员手机短信
            $noReply = \app\api\model\v1\Facts::field('unit_id,count(1) as qty')->whereNull("reply_date")->group("unit_id")->select()->toArray();
            if ($noReply) {
                    foreach ($noReply as $key=>$vf){
                        $users = Admins::where("enter_unit_id",$vf['unit_id'])->select()->toArray();
                        foreach ($users as $user){
                            if($user['mobile']){
//                              $mobile ='18602591982';
                                $mobile = $user['mobile'];
                                $qty = $vf['qty'];
                                Message::sendMessage($mobile, "", 1, $qty.'个待回复', 0);

                            }
                        }

                    }
            }
        }


//        $facts = \app\api\model\v1\Facts::select()->toArray();
//        foreach ($facts as $vf){
//            (new \app\api\model\v1\Facts())->doPlan($vf['id']);
//            var_dump($vf['id']);
//        }
//
//        //爆料的计划状态日期
//        $replys = Reply::select()->toArray();
//        foreach ($replys as $key=>$vf){
//            var_dump($vf['fact_id']."_".$vf['create_time']);
//            \app\api\model\v1\Facts::doReply($vf['fact_id'],$vf['create_time']);
//        }
//
//        //删除已删除的退回说明
//        $not_out = FactsOut::where("fact_id","not in",function ($query){
//            $query->name("facts")->whereNull("delete_time")->field("id");
//        })->select();
//        foreach ($not_out as $vf){
//            $vf->delete();
//        }


        // 指令输出
        $output->writeln('facts');
    }
}
