<?php
declare (strict_types = 1);

namespace app\api\validate\v1;

use think\Validate;

class Admin extends Validate
{
    /**
     * @return array
     * 添加或者修改用户验证规则
     */
    public static function rules($id)
    {

        $str = ($id === 0) ? '' : ',' . $id;
        return [
            'username' => 'require|min:2|max:18',
            'password' => 'requireIf:id,0|min:6|max:20',
            'email' => 'require|unique:admins,email' . $str,
            'mobile' => 'require|unique:admins,mobile' . $str . '|regex:/^1[34578][0-9]{9}$/',
            'remark' => 'max:30'
        ];
    }


    /**
     * @return array
     * 添加或者修改用户验证错误信息
     */
    public static function msg()
    {
        return [
            'mobile.require' => '手机号不能为空',
            'mobile.unique' => '手机号已存在',
            'mobile.regex' => '必须为手机号码',
            'username.require' => '用户名不能为空',
            'username.min' => '用户名长度不能小于2',
            'username.max' => '用户名长度不能超过18',
            'password.requireIf' => '密码不能为空',
            'password.min' => '密码长度不能小于6',
            'password.max' => '密码长度不能大于20',
            'remark.max' => '备注不能超过30个字符',
            'email.require'=>'邮箱不能为空',
            'email.email'=>'请输入正确的邮箱',
            'email.unique' => '邮箱已存在',
        ];
    }
}
