<?php
declare (strict_types = 1);

namespace app\api\validate\v1;

use think\Validate;

class EnterValidate extends Validate
{
    /**
     * @param $id
     * 验证条件
     * @return array
     */
    public static function rules($id){
        $str = '';
        if(0 !== $id){
            $str = ',' . $id;
        }
        return [
            'title' => 'require|max:20|unique:enter_unit,title'.$str,

        ];
    }

    /**
     * @return array
     * 错误信息
     */
    public static function msg()
    {
        return [
            'title.require' => '入驻单位名称不能为空',
            'title.unique'=>'入驻单位名称已存在',
            'title.max' =>'入驻单位名称字符长度不能超过20'
        ];

    }
}
