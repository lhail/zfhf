<?php
declare (strict_types = 1);

namespace app\api\validate\v1;

use think\Validate;

class Role extends Validate
{
    public static function rules($id)
    {
        $str = '';
        if(0 !== $id){
            $str = ',' . $id;
        }
        return [
            'title' => 'require|max:20|unique:auth_group,title' . $str,
//            'rules' => 'require',
        ];
    }

    public static function msg() {
        return [
            'title.require' => '请输入角色名称',
            'title.unique' => '此角色名称已存在',
            'title.max' => '角色名称不能超过20字符',
//            'rules.require' => '数据范围不能为空'
        ];
    }
}
