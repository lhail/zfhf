<?php
declare (strict_types = 1);

namespace app\api\validate\v1;

use think\Validate;

class Menu extends Validate
{
    /**
     * @param $id
     * 验证条件
     * @return array
     */
    public static function rules($id){
        $str = '';
        if(0 !== $id){
            $str = ',' . $id;
        }
        return [
            'title' => 'require|max:20|unique:auth_rule,title'.$str,
            'name'  =>  'require|unique:auth_rule,name',
            'sort' => 'require|integer',
            'remark' => 'max:30',
        ];
    }

    /**
     * @return array
     * 错误信息
     */
    public static function msg()
    {
        return [
            'title.require' => '菜单名称不能为空',
            'title.unique'=>'菜单名称已存在',
            'title.max' =>'菜单名称字符长度不能超过20',
            'name.require' => 'URL地址不能为空',
            'name.unique' => 'URL地址已经存在',
            'sort.require' =>'排序不能为空',
            'sort.integer' =>'排序必须为整数',
            'remark.max' =>'不能超过30个字符',
        ];

    }
}
