<?php


use think\facade\Route;

Route::post(":version.login", ':version.Login/doLogin');
Route::post(":version.work", ':version.Admin/importAdmin');
Route::post(":version.test", ':version.FactSystem/SystemTest');
//单位统计
Route::get(":version.fact_unit", ':version.Statistics/statisticsFactUnit');
//保存
Route::post(":version.save_fact", ':version.FactSystem/saveFact');
/**
    路由分组 v1/admin
 */
Route::group(':version/admin',function (){
    Route::get("admin_show", ':version.Login/adminShow');
    //获取菜单权限
    Route::get('permission', ':version.Menu/menuPermission');
    //获取菜单类别
    Route::get('menu_type', ':version.Menu/MenuType');
    //菜单列表
    Route::get('menu_list', ':version.Menu/MenuInfo');
    //新增and编辑
    Route::post('menuSave', ':version.Menu/MenuSave');
    //删除菜单
    Route::get('menuDel/:id', ':version.Menu/MenuDel');
    //添加子菜单
    Route::post('child_save', ':version.Menu/childSave');
    /**
     * 用户
     */
    //获取用户信息
    Route::get('admin_show', ':version.Login/adminShow');
    //用户列表
    Route::get('admin_list', ':version.Admin/adminList');
    //用户更新
    Route::get('update_show/:id', ':version.Admin/adminUpdateShow');
    //用户保存
    Route::post('admin_save', ':version.Admin/adminSave');
    //删除用户
    Route::get('admin_delete/:id', ':version.Admin/adminDelete');
    /**
     * 角色
     */
    //获取角色列表 代树
    Route::get('role_list', ':version.Role/roleList');
    //读取所有角色
    Route::get('role_info_list', ':version.Role/roleInfoList');
    //保存中间表角色
    Route::post('set_role_user_pivot', ':version.Role/setRoleUserPivot');
    //新增and编辑角色
    Route::post('save_role', ':version.Role/saveRole');
    //更改角色状态
    Route::put('update_role_status/:id/:status', ':version.Role/updateRoleStatus');
    //删除角色
    Route::delete('del_role/:id/:role_id', ':version.Role/delRole');
    //删除角色组
    Route::delete('del_group/:id', ':version.Role/groupDel');
    //上线单位

    //数据列表
    Route::get('online_unit_list', ':version.OnlineUnit/getOnlineUnitList');
    //删除数据
    Route::delete('online_unit_delete/:id', ':version.OnlineUnit/onlineUnitDel');
    //保存
    Route::post('save_online_unit', ':version.OnlineUnit/saveOnlineUnit');

    //获取月份
    Route::get('get_month_list', ':version.OnlineUnit/getOnlineUnitMonthList');
    //删除数据
    Route::delete('del_month/:id', ':version.OnlineUnit/onlineUnitMonthDel');
    Route::delete('del_detail/:id', ':version.OnlineUnit/onlineUnitDetailDel');

    //导入数据
    Route::post("import_excel", ':version.OnlineUnit/importOnlineUnitExcel');


    //入驻单位
    Route::post("import_enter_excel", ':version.Enter/importEnterUnitExcel');

    //入驻单位数据列表
    Route::get('enter_unit_list', ':version.Enter/getEnterUnitList');

    //保存入驻单位
    Route::post('save_enter_unit', ':version.Enter/saveEnterUnit');

    //删除入驻单位
    Route::delete('enter_unit_delete/:id', ':version.Enter/enterUnitDel');

    //当月的上线单位
    Route::get("get_program", ':version.Program/getTheMonthProgramList');

    //上传图片
    Route::post('upload_image',':version.Program/uploadImg');

    //保存预案
    Route::post('save_program_plan', ':version.Program/saveProgramPlan');

    //删除预案
    Route::delete('plan_delete/:id', ':version.Program/DelPlan');

    //查看
    Route::post("check_plan", ':version.Program/checkPlan');
    //判断预案提交条件
    Route::post("check_plan_auth", ':version.Program/checkPlanAuth');


    //媒体中心
    //获取媒体类型
    Route::get("get_media_type", ':version.Media/getMediaType');
    //删除文件
    Route::post("del_file", ':version.Media/delFile');
    //上传录音
    Route::post("upload_record", ':version.Media/uploadRecord');
    //保存 表单信息
    Route::post("save_media", ':version.Media/saveMedia');
    //获取信息
    Route::get("get_media_list", ':version.Media/getDemandList');
    //获取唯一信息
    Route::get('media_show/:id', ':version.Media/getShowMedia');
    //删除媒体
    Route::delete('media_delete/:id', ':version.Media/deleteMedia');

    /**
     * 配置 -- 轮播图
     */
    Route::post("save_carousel", ':version.System/saveCarousel');
    //列表
    Route::get("get_carousel_list", ':version.System/getCarouselList');
    //获取唯一信息
    Route::get('carousel_show/:id', ':version.System/getShowCarousel');
    //删除跑马灯
    Route::delete('carousel_delete/:id', ':version.System/deleteCarousel');
    //获取轮播图类别
    Route::get("get_carousel_type", ':version.System/getCarouseType');
    //保存预案
    Route::post('save_program_plan', ':version.Program/saveProgramPlan');
    //系统配置
    //保存配置
    Route::post("save_system_config", ':version.System/saveSystemConfig');
    //获取配置
    Route::get("get_system_config", ':version.System/getSystemConfig');
    //保存节目推荐
    Route::post("save_rec", ':version.Media/saveProgramRecommendation');
    //节目列表
    Route::get("get_pro_rec", ':version.Media/getProgramRecommendation');

    //删除节目
    Route::delete('pro_rec_delete/:id', ':version.Media/delProgramRecommendation');
    //微信用户
    Route::get("wechat_list", ':version.User/wechatList');
    //删除微信用户
    Route::get('wechat_delete/:id', ':version.User/wechatDelete');
    //随机生成账号
    Route::get("random_create_account", ':version.User/randomCreateAccount');

    //获取投诉列表
    Route::get('get_fact_list', ':version.FactSystem/getFactList');
    //获取唯一信息
    Route::get('get_fact_show', ':version.FactSystem/getFactShowInfo');
    //回复
    Route::post('reply_to_comment',':version.FactSystem/replyToComment');
    //删除
    Route::delete('del_fact/:id', ':version.FactSystem/deleteFact');

    //案件退回
    Route::post("out_fact", ':version.FactSystem/outFact');
    //获取退回列表
    Route::get("get_out_fact_list", ':version.FactSystem/getOutFactList');
    //同意退回
    Route::post("agree_out_fact", ':version.FactSystem/agreeOutFact');
    //拒绝
    Route::post("refuse_out_fact", ':version.FactSystem/refuseOutFact');
    Route::get("fact_tongji", ':version.FactSystem/tongji');


    //统计api
    Route::get('statistics_fact',':version.Statistics/statisticsFact');
    //分值列表
    Route::get('get_fact_score', ':version.Statistics/statisticsFactScore');
    //更改分值
    Route::post('change_score',':version.Statistics/changeScore');
    //单位统计
    Route::get("fact_unit", ':version.Statistics/statisticsFactUnit');
    //上线率
    Route::get("statistics_unit_online", ':version.Statistics/statisticsUnitOnline');
    //下载数据模板文件
    Route::get('download_file/:id', ':version.System/downloadFile');
    //处理案件是否显示
    Route::get('is_show_fact/:id', ':version.FactSystem/isShowFact');
    //处理案件是否关闭
    Route::get('is_close_fact/:id', ':version.FactSystem/isCloseFact');
    //获取爆料类别
    Route::get('fact_type', ':version.FactSystem/factType');
    //保存
    Route::post("save_batch_fact", ':version.FactSystem/batchSaveFact');
    //保存
    Route::post("check_user_mobile", ':version.FactSystem/checkUserMobile');
//单位获取
    Route::get('unit_all', ':version.OnlineUnit/unitAllList');

})->middleware(\app\api\middleware\v1\AuthAdminCheck::class);

/**
 * 路由分组 手机端v1/wx
 */
Route::group(':version/wx',function (){
    //获取轮播图
    Route::get("get_carousel_list", ':version.System/getCarouselList');
    //一周上线单位
    Route::get("get_week_online_unit", ':version.OnlineUnit/getWeekOnlineUnit');
    //获取当天上线的单位
    Route::get("get_today_online_unit", ':version.OnlineUnit/todayOnlineUnit');
    //获取媒体
    Route::get("get_media_list", ':version.Media/getDemandList');
    //获取入驻单位
    Route::get('enter_unit_list', ':version.Enter/getEnterUnitList');
    //获取全年上线计划
    Route::get('yearly_online_plan', ':version.Program/yearlyOnlinePlan');
    //获取所有的媒体列表
    Route::get('get_media_all_list', ':version.System/getMediaAllList');
    //获取唯一媒体信息
    Route::get('media_show_info', ':version.Media/getShowMediaInfo');

    //获取投诉列表
    Route::get('get_fact_list', ':version.FactSystem/getFactList');
    //获取唯一信息
    Route::get('get_fact_show', ':version.FactSystem/getFactShowInfo');
    //获取关于我们
    Route::get('get_about_me',':version.System/getAboutMeInfo');

    //获取token
    Route::post('get_token', ':version.User/getToken');   //小程序获取用户token
    //验证token
    Route::post('token_verify', ':version.User/tokenVerify');
    //短信验证码
    Route::post('get_sms_code',':version.User/getSmsCode');

    //获取模版消息
    Route::get('get_template_id', ':version.Message/getTemplateId');

    //发送模版消息
    Route::post('save_audio',':version.Message/SaveAudio');
    //删除文件
    Route::post("del_file", ':version.Media/delFile');
    //获取类型总数
    Route::get("get_fact_count", ':version.Message/getFactCount');
    //获取配置
    Route::get("get_system_config", ':version.System/getSystemConfig');
    //节目列表
    Route::get("get_pro_rec", ':version.Media/getProgramRecommendation');
});

/**
 *
 * 需要验证的微信api接口  中间件
 */
Route::group(':version/auth_wx',function (){
    //下方的都是需要登录后才可操作的api接口,由于登录暂未写，先测试用
    //获取爆料类别
    Route::get('fact_type', ':version.FactSystem/factType');
    //获取爆料方式
    Route::get('fact_way', ':version.FactSystem/factWay');
    //保存
    Route::post("save_fact", ':version.FactSystem/saveFact');
    //上传照片
    Route::post('upload_fact_img',':version.FactSystem/uploadFactImg');
    //发表评论
    Route::post('save_comment',':version.FactSystem/saveComment');
    //打分并且结束
    Route::post('save_score_and_over',':version.FactSystem/saveScoreAndOver');
    //管理者登录
    Route::post('wx_management_login',':version.User/wxManagementLogin');
    //手机号验证
    Route::post('mobile_verify', ':version.User/verifyMobile');
    //校验验证码
    Route::post('check_sms_code',':version.User/checkSmsCode');
    //回复
    Route::post('reply_to_comment',':version.FactSystem/replyToComment');

    //当月的上线单位
    Route::get("get_program", ':version.Program/getTheMonthProgramList');
    //上传预案
    Route::post('upload_program_plan',':version.FactSystem/uploadPlan');
    //保存预案
    Route::post('save_program_plan', ':version.Program/saveProgramPlan');
    //判断预案提交条件
    Route::post("check_plan_auth", ':version.Program/checkPlanAuth');
    //案件退回
    Route::post("out_fact", ':version.FactSystem/outFact');
    //获取退回列表
    Route::get("get_out_fact_list", ':version.FactSystem/getOutFactList');
    //同意退回
    Route::post("agree_out_fact", ':version.FactSystem/agreeOutFact');
    //拒绝
    Route::post("refuse_out_fact", ':version.FactSystem/refuseOutFact');
    //获取案件时间线
    Route::post("get_time_fact_line", ':version.FactSystem/getTimeFactLine');
    //显示当天的上线单位
    Route::get("online_unit_today", ':version.OnlineUnit/todayOnline');
    //保存一把手上线
    Route::post("save_online_count", ':version.OnlineUnit/saveOnlineCount');
    //验证密码
    Route::post("check_password", ':version.Admin/checkPassword');
    //修改密码
    Route::post("update_password", ':version.Admin/updatePassword');

     //获取隐藏列表
    Route::get("is_hidden_type", ':version.FactSystem/IsHiddenType');
    //删除预案
    Route::post('plan_delete', ':version.Program/DelWXPlan');
})->middleware(\app\api\middleware\v1\AuthWxCheck::class);

Route::get(":version.reportadmin_facts", ':version.reportadmin/facts');