<?php
declare (strict_types = 1);

namespace app\api\command\v1;

use app\api\model\v1\Users;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

class ClearUser extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('clearUser')
            ->setDescription('the clearUser command');
    }

    protected function execute(Input $input, Output $output)
    {

        Users::chunk(100, function ($users) use ($output){
            foreach ($users as $user) {
               $data = $user->toArray();
               if(empty($data['nickname']) && empty($data['mobile'])){
                   $output->writeln(sprintf('正在删除 ID 为 %s 的数据', $data['id']));
                   //->force()
                   $user->delete();
                   $output->writeln(sprintf('正在删除完成'));
               }
            }
        });
        // 指令输出
        $output->writeln('删除完成');
    }
}
