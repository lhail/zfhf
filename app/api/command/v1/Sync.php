<?php
declare (strict_types = 1);

namespace app\api\command\v1;

use app\api\model\v1\Facts;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

class Sync extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('sync')
            ->setDescription('the sync command');
    }

    protected function execute(Input $input, Output $output)
    {


        Facts::field('id,title,user_id,fact_type,filter_content as note,accessory_path,address as area_code_name,source as source_code_name,is_sync')
            ->with(['user'])
            ->where(['is_sync'=>1])
            ->chunk(100, function ($facts) use ($output){
            foreach ($facts as $fact) {
                //转为数组
                $data = $fact->toArray();
                $data['like_name'] = $fact->user->nickname ?? '';
                $data['like_phone'] = $fact->user->mobile ?? '';
                $data['classify_code_name'] = Facts::$fact_type[$fact['fact_type']] ?? '';
                $output->writeln(sprintf('正在同步 ID 为 %s 的数据', $fact->id));
                $fact->save(['is_sync'=>2]);
                halt($data);
                //同步前置库
                \think\facade\Db::connect('demo')
                    ->table('zcj_qzk')
                    ->save($data);
                //修改当前字段
                $fact->save(['is_sync'=>2]);
            }
        });

        // 指令输出
        $output->writeln('sync');
    }
}
