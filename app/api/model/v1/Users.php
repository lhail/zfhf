<?php
declare (strict_types = 1);

namespace app\api\model\v1;

use think\Model;

/**
 * @mixin \think\Model
 */
class Users extends AppModel
{
    protected $hidden = ['password'];
    //
    const IS_MANAGEMENT_ZERO = 0; //游客
    const IS_MANAGEMENT_ONE = 1; //管理人员

    //权限(1,主持人(超级管理员),2单位负责人权限,3游客)
    const IS_AUTH_ONE = 1; //主持人(超级管理员)
    const IS_AUTH_TWO = 2; //单位负责人权限
    const IS_AUTH_THREE = 3; //游客

    //身份识别
    const TYPE_ONE = 1; //管理者
    const TYPE_TWO = 2; //游客

    public static $is_auth_type = array(
        self::IS_AUTH_ONE => '主持人',
        self::IS_AUTH_TWO=>'单位负责人',
        self::IS_AUTH_THREE =>'游客'
    );


    public function admins(){
        return $this->belongsTo(Admins::class,'admin_id');
    }


    public function unit(){
        return $this->belongsTo(EnterUnit::class,'unit_id');
    }


}
