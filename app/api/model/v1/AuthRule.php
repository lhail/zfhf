<?php
declare (strict_types = 1);

namespace app\api\model\v1;

use app\api\model\v1\AppModel;
use think\Model;

/**
 * @mixin \think\Model
 */
class AuthRule extends AppModel
{
    //

    //菜单类型
    const TYPE_ONE = 1;
    const TYPE_TWO = 2;

    public static $is_type = array(
        self::TYPE_ONE => '系统功能',
        self::TYPE_TWO => '用户功能',
    );


    const OPEN_ONE = 1;
    const OPEN_TWO = 0;

    public static $is_open = array(
        self::OPEN_ONE => '启用',
        self::OPEN_TWO => '禁用'
    );

    /**
     * @param     $array
     * @param int $pid
     * 递归菜单数据
     *
     * @return array
     */
    public static function sortMenu($array, $pid = 0)
    {
        $arr = array();
        foreach ($array as $v) {
            if ($v['parent_id'] == $pid) {
                $temp = self::sortMenu($array, $v['id']);
                //判断是否存在子数组
                if ($temp) {
                    $v['child'] = $temp;
                }
                $arr[] = $v;
            }
        }
        return $arr;
    }
}
