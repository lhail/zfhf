<?php
declare (strict_types=1);

namespace app\api\model\v1;

use think\Model;

/**
 * @mixin \think\Model
 */
class OnlineUnitDetail extends AppModel
{
    //
    const ONLINE_STATUS_ONE = 1; //上线
    const ONLINE_STATUS_TWO = 2; //回复版
    const ONLINE_STATUS_THREE = 3; //不上线

    const ONLINE_STATUS_ONE_STRING = '上线'; //上线
    const ONLINE_STATUS_TWO_STRING = '回复版'; //回复版
    const ONLINE_STATUS_THREE_STRING = '不上线'; //不上线

    public static $online_status_string = array(
        self::ONLINE_STATUS_ONE_STRING => 1,
        self::ONLINE_STATUS_TWO_STRING => 2,
        self::ONLINE_STATUS_THREE_STRING => 3,
    );
    public static $online_status = array(
        self::ONLINE_STATUS_ONE => '上线',
        self::ONLINE_STATUS_TWO => '回复版',
        self::ONLINE_STATUS_THREE => '不上线',
    );


    public function programPlan(){
        return $this->hasOne(ProgramPlan::class,'online_unit_id')->field('id,online_unit_id,accessory_status,accessory_path,source');
    }

    public function Rcount(){
        return $this->hasOne(OnlineUnitCount::class,'replace_before_unit_id');
    }

    /**
     * 根据 日期 计算之后的7个工作日是哪一天
     * 默认 加2天双休日，否则用上线日期做区别
     * @param $num
     * @return void
     */
    public static function doNextDay($day,$num=7){
        $lists = self::where("online_date",">",$day)->where("online_status","<",3)->limit(0,$num)->select()->toArray();
        if(count($lists)==$num){
            $end = end($lists);
            $day = $end['online_date'];
        }else{
            $qty = $num+2;
            $day =date("Y-m-d",strtotime("+{$qty} days",strtotime($day)));
        }
        return $day;
    }
}
