<?php
declare (strict_types = 1);

namespace app\api\model\v1;

use think\Model;
use think\model\Pivot;

/**
 * @mixin \think\Model
 */
class GroupUserPivot extends Pivot
{
    protected $name = 'auth_group_access';

    /**
     * @param $user
     * @param $role_id
     * @User: 刘海龙
     * @Date: 2021/7/22
     * @Time: 15:32
     * 关联关系
     */
    public static function roleUserSave($user,$role_id)
    {
        $role_id = explode(',',$role_id);
        $user->roles()->detach();
        $user->roles()->attach($role_id);
    }
}
