<?php
declare (strict_types = 1);

namespace app\api\model\v1;

use think\Model;

/**
 * @mixin \think\Model
 */
class OnlineUnitMonth extends AppModel
{
    //

    public function details(){
        return $this->hasMany(OnlineUnitDetail::class,'online_unit_month_id');
    }
}
