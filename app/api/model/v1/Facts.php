<?php
declare (strict_types=1);

namespace app\api\model\v1;

use think\Model;

/**
 * @mixin \think\Model
 */
class Facts extends AppModel
{
    protected $hidden = ['content'];

    const FACT_TYPE_ONE = 1;
    const FACT_TYPE_TWO = 2;
    const FACT_TYPE_THREE = 3;
    const FACT_TYPE_FOUR = 4;

    /**
     * @var string[]
     * @User: 刘海龙
     * @Date: 2021/9/1
     * @Time: 17:53
     * 爆料类型
     */
    public static $fact_type = array(
        self::FACT_TYPE_ONE => '投诉',
        self::FACT_TYPE_TWO => '建议',
        self::FACT_TYPE_THREE => '咨询',
        self::FACT_TYPE_FOUR => '其他'
    );


    const FACT_WAY_ONE = 1;
    const FACT_WAY_TWO = 2;
    const FACT_WAY_THREE = 3;
    const FACT_WAY_FOUR = 4;
    /**
     * @var string[]
     * @User: 刘海龙
     * @Date: 2021/9/1
     * @Time: 17:55
     * 爆料方式
     */
    public static $fact_way = array(
        self::FACT_WAY_ONE => '音频',
        self::FACT_WAY_TWO => '视频',
        self::FACT_WAY_THREE => '图片',
    );


    const IS_EXIST_ZERO = 0; //不存在图片或者视频或者音频
    const IS_EXIST_ONE = 1;//存在图片或者视频或者音频

    const IS_FINISH_ZERO = 0; // 案件未结束
    const IS_FINISH_ONE = 1;// 案件结束

    const STATUS_ZERO = 0; //默认
    const STATUS_ONE = 1; //精选


    //案件是否属于我自己的常量
    const IS_OUT_ZER0 = 0; //属于我的案件
    const IS_OUT_ONE = 1; //不属于我的案件
    const IS_OUT_TWO = 2; //拒绝退回
    const IS_OUT_THREE = 3; //同意退回


    const IS_SHOW_ONE = 1; // 默认 (显示)
    const IS_SHOW_TWO = 2; //隐藏(前端不显示)

    const IS_CLOSE_ONE = 1; // 默认 (开启)
    const IS_CLOSE_TWO = 2; //关闭(关闭)


    const IS_HIDDEN_ONE = 1; //显示
    const IS_HIDDEN_TWO = 2; //隐藏

    public static $is_hidden = array(
        self::IS_HIDDEN_ONE => '显示',
        self::IS_HIDDEN_TWO => '隐藏',
    );

    public function unit()
    {
        return $this->belongsTo(EnterUnit::class, 'unit_id')->field('id,title');
    }

    //评论
    public function comment()
    {
        return $this->hasMany(Comments::class, 'fact_id');
    }

    public function replyCount()
    {
        return $this->hasMany(Reply::class, 'fact_id');
    }

    //分值
    public function score()
    {
        return $this->hasOne(FactScore::class, 'fact_id')->field('id,fact_id,score');
    }

    //关联用户
    public function user()
    {
        return $this->belongsTo(Users::class, 'user_id')->field('id,nickname,head_img,mobile');
    }

    public function out()
    {
        return $this->hasMany(FactsOut::class, 'fact_id');
    }

    /**
     * 根据爆料 确定计划日期：7个工作日
     * 2023-7-8 采用上线日期计算
     * @param $fact_id
     * @return void
     */
    public static function doPlan($fact_id = 0, $day = '')
    {
        $model = self::where("id",$fact_id)->find();
        if ($model) {
            if (!$day) {
                $day = date("Y-m-d", strtotime($model['create_time']));
            }
            $plan_date = OnlineUnitDetail::doNextDay($day, 7);
            $model->plan_date = $plan_date;
            $model->save();
        } else {
            if (!$day) {
                $day = date("Y-m-d");
            }
            $plan_date = OnlineUnitDetail::doNextDay($day, 7);
        }
        return $plan_date;
    }

    /**
     * 2023-7-9 回复时间以及计划状态修改
     * @param $fact_id
     * @param $reply_date
     */
    public static function doReply($fact_id, $reply_date)
    {
        $model = self::where("id",$fact_id)->find();
        if($model){
            $model->reply_date = $reply_date;
            if(!$model->plan_date){
                $day = date("Y-m-d",strtotime($model->create_time));
                $plan_date = OnlineUnitDetail::doNextDay($day);
                $model->plan_date = $plan_date;
            }else{
                $plan_date = $model->plan_date;
            }
            $replyDay = date("Y-m-d",strtotime($reply_date));
            if($replyDay<=$plan_date){
                $model->plan = 1;
            }else{
                $model->plan = 2;
            }
            $model->save();

        }
    }
}
