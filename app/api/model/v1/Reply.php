<?php
declare (strict_types = 1);

namespace app\api\model\v1;

use think\Model;

/**
 * @mixin \think\Model
 */
class Reply extends AppModel
{
    protected $hidden = ['reply_content'];
    //
    public function user(){
        return $this->belongsTo(Users::class,'reply_user_id')->field('id,nickname,unit_id');
    }
}
