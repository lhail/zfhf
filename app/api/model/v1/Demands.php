<?php
declare (strict_types=1);

namespace app\api\model\v1;

use think\Model;

/**
 * @mixin \think\Model
 */
class Demands extends AppModel
{
    //

    const MEDIA_TYPE_ONE = 1; //直播
    const MEDIA_TYPE_TWO = 2; //音频
    const MEDIA_TYPE_THREE = 3; //视频


    public static $media_type = array(
        self::MEDIA_TYPE_ONE => '直播',
        self::MEDIA_TYPE_TWO => '音频',
        self::MEDIA_TYPE_THREE => '视频'
    );

    const IS_ACCESSORY_ZERO = 0; //默认不存在附件
    const IS_ACCESSORY_ONE = 1; //存在附件
}
