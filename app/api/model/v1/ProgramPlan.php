<?php
declare (strict_types = 1);

namespace app\api\model\v1;

use think\Model;

/**
 * @mixin \think\Model
 */
class ProgramPlan extends AppModel
{
    const IS_STATUS = 0;
    const IS_STATUS_ONE = 1; //已传预案


    //
    public function plans()
    {
        return $this->belongsToMany(OnlineUnitDetail::class,'program_plan_unit_access','program_plan_id','on_unit_id');
    }
}
