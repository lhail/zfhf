<?php
declare (strict_types = 1);

namespace app\api\model\v1;

use think\Model;

/**
 * @mixin \think\Model
 */
class EnterUnit extends AppModel
{
    //

    /**
     * @param $arrData
     * @param $create_id
     * @User: 刘海龙
     * @Date: 2021/8/26
     * @Time: 8:43
     * @return bool
     * 保存Excel
     */
    public static function saveEnterUnitExcel($arrData,$create_id){

        if (!empty($arrData)) {
            foreach ($arrData as $key=>$v){
                self::create(['title'=>$v[0] ?? '','create_id'=>$create_id]);
            }
            $result  =  true;
        }else{
            $result  =  false;
        }
        return $result;
    }


    public function facts(){
        return $this->hasMany(Facts::class,'unit_id');
    }

    public function OnlineUnitCount(){
        return $this->hasMany(OnlineUnitCount::class,'replace_after_unit_id');
    }
}
