<?php
declare (strict_types=1);

namespace app\api\model\v1;

use think\Model;


class Admins extends AppModel
{
    protected $hidden = ['password'];

    public function roles()
    {
        return $this->belongsToMany(AuthGroup::class, 'auth_group_access', 'group_id', 'uid');
    }


    public function unit()
    {
        return $this->belongsTo(EnterUnit::class, 'enter_unit_id')->field('id,title');
    }


    public static function saveAdminExcel($arrData, $create_id)
    {
        if (!empty($arrData)) {
            foreach ($arrData as $key => $v) {
                $is_ex = self::where(['mobile'=>$v[1]])->find();
                if (is_null($is_ex)) { //为空的时候新增
                    self::create([
                        'username' => $v[0] ?? '',
                        'mobile' => $v[1] ?? '',
                        'email' => $v[1] . '@139.com' ?? '',
                        'password' => password_hash('111111', PASSWORD_DEFAULT),
                        'enter_unit_id' => $v[2] ?? '',
                        'create_id' => $create_id
                    ]);
                }
            }
            $result = true;
        } else {
            $result = false;
        }
        return $result;
    }
}
