<?php
declare (strict_types = 1);

namespace app\api\model\v1;

use think\Model;

/**
 * @mixin \think\Model
 */
class SystemConfig extends AppModel
{
    //

    const DAY_TYPE_ONE = 1; // 按照天数提交
    const DAY_TYPE_TWO = 2; //按照工作日提交
}
