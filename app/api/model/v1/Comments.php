<?php
declare (strict_types = 1);

namespace app\api\model\v1;

use think\Model;

/**
 * @mixin \think\Model
 */
class Comments extends AppModel
{
    protected $hidden = ['comment_content'];

    //回复
    public function reply(){
        return $this->hasMany(Reply::class,'comment_id');
    }


    public function user(){
        return $this->belongsTo(Users::class,'user_id')->field('id,nickname,head_img');
    }
}
