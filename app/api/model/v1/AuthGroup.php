<?php
declare (strict_types = 1);

namespace app\api\model\v1;

use app\api\model\v1\AppModel;
use think\Model;

/**
 * @mixin \think\Model
 */
class AuthGroup extends AppModel
{
    //
}
