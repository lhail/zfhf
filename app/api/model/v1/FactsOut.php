<?php
declare (strict_types = 1);

namespace app\api\model\v1;

use think\Model;

/**
 * @mixin \think\Model
 */
class FactsOut extends AppModel
{
    //


    const OUT_ZERO = 0; //暂未分配 初始化
    const OUT_ONE = 1; //同意退回
    const OUT_TWO = 2; //拒绝退回
    const OUT_THREE = 3; //同意退回后重新分配


    public static $fact_is_out = array(
        self::OUT_ZERO => '退回中...',
        self::OUT_ONE => '同意退回',
        self::OUT_TWO => '拒绝退回',
        self::OUT_THREE => '重新分配',
    );
    public function facts(){
        return $this->belongsTo(Facts::class,'fact_id');
    }

    public function unit(){
        return $this->belongsTo(EnterUnit::class,'out_unit_id')->field('id,title');
    }
}
