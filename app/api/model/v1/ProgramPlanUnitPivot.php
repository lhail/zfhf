<?php
declare (strict_types = 1);

namespace app\api\model\v1;

use think\Model;
use think\model\Pivot;

/**
 * @mixin \think\Model
 */
class ProgramPlanUnitPivot extends Pivot
{
    //
    protected $name = 'program_plan_unit_access';
}
