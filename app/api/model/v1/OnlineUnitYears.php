<?php
declare (strict_types=1);

namespace app\api\model\v1;

use think\Model;

/**
 * @mixin \think\Model
 */
class OnlineUnitYears extends AppModel
{


    //月份
    public function onlineUnitMonths()
    {
        return $this->hasMany(OnlineUnitMonth::class, 'online_unit_years_id');
    }

    //详细
    public function onlineUnitDetails()
    {
        return $this->hasMany(OnlineUnitDetail::class, 'online_unit_years_id');
    }

    /**
     * @param $arrData
     * @param $create_id
     * @User: 刘海龙
     * @Date: 2021/8/25
     * @Time: 15:42
     * @return bool
     *  导入保存数据
     */
    public static function saveOnlineUnit($arrData, $create_id)
    {
        // halt(json_encode($arrData, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
        if (!empty($arrData)) {
            $res = self::where(['years' => date('Y')])->find();
            if (!is_null($res)) {
                $result = 403;
            } else {
                foreach ($arrData as $key => $value) {
                    $model_id = self::create([
                        'years_title' => $value['years_title'] ?? '',
                        'years' => date('Y'),
                        'create_id' => $create_id
                    ])->id;
                    if (isset($value['item'])) { //判断是否包含item字段
                        //如果包含item字段 循环
                        foreach ($value['item'] as $child_key => $child_value) {
                            $online_unit_month_id = OnlineUnitMonth::create([
                                'month_title' => $child_value['month_title'] ?? '',
                                'online_unit_years_id' => $model_id ?? 0,
                                'create_id' => $create_id ?? 0,
                            ])->id;
                            if (isset($child_value['item'])) {
                                foreach ($child_value['item'] as $child_child_key => $child_child_value) {

                                    OnlineUnitDetail::create([
                                        'detail_title' => $child_child_value['detail_title'] ?? '',
                                        'online_unit_years_id' => $model_id ?? '',
                                        'online_unit_month_id' => $online_unit_month_id ?? '',
                                        'online_date' => $child_child_value['online_date'] ?? '',
                                        'online_week' => $child_child_value['online_week'] ?? '',
                                        'create_id' => $create_id ?? 0,
                                        'remark' => $child_child_value['remark'] === '无' ? '' : $child_child_value['remark'],
                                        'online_status' => OnlineUnitDetail::$online_status_string[trim($child_child_value['online_status'])] ?? 0
                                    ]);
                                }
                            }
                        }
                    }
                }
                $result = 200;
            }
        } else {
            $result = 500;
        }
        return $result;
    }
}
