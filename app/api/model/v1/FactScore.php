<?php
declare (strict_types = 1);

namespace app\api\model\v1;

use think\Model;

/**
 * @mixin \think\Model
 */
class FactScore extends AppModel
{
    //


    public function fact(){
        return $this->belongsTo(Facts::class,'fact_id')->field('id,is_finish,unit_id,fact_type');
    }
}
