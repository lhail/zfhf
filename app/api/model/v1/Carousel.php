<?php
declare (strict_types = 1);

namespace app\api\model\v1;

use think\Model;

/**
 * @mixin \think\Model
 */
class Carousel extends AppModel
{
    //
    const CAROUSEL_TYPE_ONE = 1; //首页
    const CAROUSEL_TYPE_TWO = 2; //内页
    const CAROUSEL_TYPE_THREE = 3; //爆料页

    public static $carouse_type = array(
        self::CAROUSEL_TYPE_ONE=>'首页',
        self::CAROUSEL_TYPE_TWO=>'点播页',
        self::CAROUSEL_TYPE_THREE=>'爆料页'
    );
}
