<?php
declare (strict_types=1);

namespace app\api\controller\v1;

use app\api\model\v1\EnterUnit;
use app\api\model\v1\Facts;
use app\api\model\v1\FactScore;
use app\api\model\v1\OnlineUnitCount;
use app\BaseController;
use think\Request;

class Statistics extends BaseController
{

    /**
     * @User: 刘海龙
     * @Date: 2021/9/13
     * @Time: 9:19
     * @return \think\response\Json
     * 统计4大分类的总数
     */
    public function statisticsFact()
    {
        //->where(['is_finish'=>Facts::IS_FINISH_ZERO]) 是否区分已评分和未评分?
        $res = Facts::field('count(*) as count,fact_type')->group('fact_type')->select()->toArray();
        foreach ($res as $key => $v) {
            $res[$key]['fact_type_name'] = Facts::$fact_type[$v['fact_type']];
        }
        return $this->response($res);
    }

    /**
     * @User: 刘海龙
     * @Date: 2021/9/13
     * @Time: 9:44
     * @return \think\response\Json
     * @throws \think\db\exception\DbException
     * 分值列表
     */
    public function statisticsFactScore()
    {
        $limit = $this->request->param('limit', 10);
        $res = FactScore::with(['fact' => ['unit']])->paginate($limit)->each(function ($item, $key) {
            $item['fact_type_name'] = Facts::$fact_type[$item['fact']['fact_type']] ?? '';
            return $item;
        });
        return $this->response($res);
    }

    /**
     * @User: 刘海龙
     * @Date: 2021/9/13
     * @Time: 11:04
     * @return \think\response\Json
     * 更改分值
     */
    public function changeScore()
    {
        $data = $this->request->param();
        FactScore::update($data);
        return $this->response();
    }


    /**
     * @User: 刘海龙
     * @Date: 2021/9/13
     * @Time: 14:41
     * @return \think\response\Json
     * @throws \think\db\exception\DbException
     * 单位统计 爆料类型(1投诉,2建议,3咨询,4其他)
     */
    public function statisticsFactUnit()
    {
        $limit = $this->request->param('limit', 10);
        $res = EnterUnit::withCount(['facts'=>function($q){
            $q->whereYear('create_time');
            return $q;
        }])->withCount(['facts' => function ($query,&$ailed) {
            $query->where(['fact_type'=>Facts::FACT_TYPE_ONE])->whereYear('create_time');
            $ailed = 'fact_type_one'; //投诉
            return $query;
        }])->withCount(['facts' => function ($query,&$ailed) {
            $query->where(['fact_type'=>Facts::FACT_TYPE_TWO])->whereYear('create_time');
            $ailed = 'fact_type_two'; //建议
            return $query;
        }])->withCount(['facts' => function ($query,&$ailed) {
            $query->where(['fact_type'=>Facts::FACT_TYPE_THREE])->whereYear('create_time');
            $ailed = 'fact_type_three'; //咨询
            return $query;
        }])->withCount(['facts' => function ($query,&$ailed) {
            $query->where(['fact_type'=>Facts::FACT_TYPE_FOUR])->whereYear('create_time');
            $ailed = 'fact_type_four'; //其他
            return $query;
        }])->paginate($limit);
        return $this->response($res);
    }

    /**
     * @User: 刘海龙
     * @Date: 2021/9/13
     * @Time: 16:25
     * @return \think\response\Json
     * @throws \think\db\exception\DbException
     * 上线率
     */
    public function statisticsUnitOnline()
    {

        $limit = $this->request->param('limit', 20);
        $res = EnterUnit::withCount('OnlineUnitCount')->with(['OnlineUnitCount' => function ($query) {
            $query->field('count(is_online) as count,replace_after_unit_id,is_online,group_concat(date_format(create_time,"%Y-%m-%d")) as times')->group('replace_after_unit_id,is_online');
        }])->paginate($limit)->each(function ($item, $key) {
            $pinj = '';
            foreach ($item['OnlineUnitCount'] as $k => $v) {
                if ($v['is_online'] === 2) {
                    $item['ybs'] = $v['count'] ?? '';
                } else {
                    $item['ybs'] = 0;
                }
                $pinj = $v['times'];
            }
            $item['ybs_time'] = $pinj;
            return $item;
        });
        return $this->response($res);
    }
}
