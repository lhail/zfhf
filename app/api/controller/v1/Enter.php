<?php
declare (strict_types=1);

namespace app\api\controller\v1;

use app\api\helpers\v1\traits\FileUpload;
use app\api\helpers\v1\traits\SearchDataForModel;
use app\api\model\v1\EnterUnit;
use app\api\validate\v1\EnterValidate;
use app\BaseController;
use think\exception\ValidateException;
use think\Request;

class Enter extends BaseController
{
    use SearchDataForModel, FileUpload;

    /**
     * @User: 刘海龙
     * @Date: 2021/8/26
     * @Time: 8:49
     * @return \think\response\Json
     * @throws \think\db\exception\DbException
     * 数据列表
     */
    public function getEnterUnitList()
    {
        $map = [];
        if ($this->request->has('title') && !empty($this->request->param('title'))) {
            $map[] = ['title', 'like', '%' . $this->request->param('title') . '%'];
        }
        $limit = $this->request->param('limit', 10);
        $type = $this->request->param('type', 'paginate');
        if ($type === 'paginate') {
            $res = $this->search(new EnterUnit(), $map, $limit);
        } else {
            $res = EnterUnit::field('id,title')->select()->toArray();
        }

        return $this->response($res);
    }

    /**
     * @User: 刘海龙
     * @Date: 2021/8/26
     * @Time: 8:48
     * @return \think\response\Json
     * 导入数据
     */
    public function importEnterUnitExcel()
    {
        if (!$this->request->file('file')) {
            return $this->response(501, '请选择上传文件');
        }
        $file = $this->request->file('file');

        $result = $this->saveImportExcel($file, 'import_excel', 'stage');
        $is_bool = EnterUnit::saveEnterUnitExcel($result, $this->request->uid);
        if ($is_bool) {
            return $this->response();
        } else {
            return $this->response(403, '导入问卷失败');
        }
    }

    /**
     * @User: 刘海龙
     * @Date: 2021/8/26
     * @Time: 9:00
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 保存and修改
     */
    public function saveEnterUnit()
    {
        $id = $this->request->param('id', 0);
        $data = $this->request->param();
        try {
            $validator = $this->validate($data, EnterValidate::rules($id), EnterValidate::msg());
            if ($validator) {
                $data['create_id'] = $this->request->uid; //用户id
                if (intval($id) === 0) {
                    EnterUnit::create($data);
                } else {
                    $res = EnterUnit::find($id);
                    if (is_null($res)) {
                        return $this->response(403, '数据不存在');
                    }
                    $res->save($data);
                }
                return $this->response();
            }
        } catch (ValidateException $exception) {
            // 验证失败 输出错误信息
            return $this->response(403, $exception->getError());
        }
    }

    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/8/26
     * @Time: 9:11
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 删除
     */
    public function enterUnitDel($id)
    {
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            $res = EnterUnit::find($id);
            if (is_null($res)) {
                return $this->response(404, '未找到该数据');
            }
            $res->delete();
        }
        return $this->response();
    }
}
