<?php
declare (strict_types=1);

namespace app\api\controller\v1;

use app\api\helpers\v1\traits\FileUpload;
use app\api\helpers\v1\traits\SearchDataForModel;
use app\api\helpers\v1\traits\WeekDay;
use app\api\model\v1\Carousel;
use app\api\model\v1\Demands;
use app\api\model\v1\Facts;
use app\api\model\v1\OnlineUnitDetail;
use app\api\model\v1\OnlineUnitMonth;
use app\api\model\v1\OnlineUnitYears;
use app\api\model\v1\SystemConfig;
use app\BaseController;
use think\facade\Db;
use think\facade\Env;
use think\Request;

class System extends BaseController
{
    use SearchDataForModel, WeekDay,FileUpload;

    /**
     * @User: 刘海龙
     * @Date: 2021/8/30
     * @Time: 14:37
     * @return \think\response\Json
     * @throws \think\db\exception\DbException
     * 获取轮播列表
     */
    public function getCarouselList()
    {
        $limit = $this->request->param('limit', 10);
        $map = [];
        if ($this->request->has('carousel_type') && !empty($this->request->param('carousel_type'))) {
            $map[] = ['carousel_type', '=', $this->request->param('carousel_type')];
        }
        $res = Carousel::where($map)->paginate($limit)->each(function ($item, $key) {
            $item['url_path'] = Env::get('HTTP.http', 'HTTP.default_http') . 'storage/' . $item['path'];
            return $item;
        });
        return $this->response($res);
    }

    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/8/30
     * @Time: 14:47
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 获取唯一信息
     */
    public function getShowCarousel($id)
    {
        $res = Carousel::find($id);
        if (is_null($res)) {
            return $this->response(403, '找不到该资源信息');
        }
        $file = app()->getRootPath() . 'public/storage/' . $res->path;
        if (file_exists($file)) {
            $url = Env::get('HTTP.http', 'HTTP.default_http') . 'storage/' . $res->path;
        } else {
            $url = Env::get('HTTP.http', 'HTTP.default_http') . $res->path;
        }
        $data[] = ['name' => $res->title ?? '暂未', 'url' => $url, 'file' => $res->path];
        $res['img_fileList'] = $data;
        $res['carousel_type'] = $res->carousel_type . '';
        return $this->response($res->toArray());
    }

    /**
     * @User: 刘海龙
     * @Date: 2021/9/2
     * @Time: 18:58
     * @return \think\response\Json
     * 获取轮播图类别
     */
    public function getCarouseType(){
        $res = Carousel::$carouse_type;
        return $this->response($res);
    }

    /**
     * @User: 刘海龙
     * @Date: 2021/8/30
     * @Time: 14:30
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 保存轮播图
     */
    public function saveCarousel()
    {
        $id = $this->request->param('id', 0);
        $data = $this->request->param();
        $data['create_id'] = $this->request->uid; //用户id
        if (intval($id) === 0) {
            Carousel::create($data);
        } else {
            $res = Carousel::find($id);
            if (is_null($res)) {
                return $this->response(403, '数据不存在');
            }
            $res->save($data);
        }
        return $this->response();
    }

    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/8/30
     * @Time: 14:52
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 删除
     */
    public function deleteCarousel($id)
    {
        $res = Carousel::find($id);
        if (is_null($res)) {
            return $this->response(403, '找不到该资源信息');
        }
        if (!empty($res->path)) {
            $file = app()->getRootPath() . 'public/storage/' . $res->path;
            if (file_exists($file)) {
                unlink($file);
            }
        }
        $res->force()->delete();
        return $this->response();
    }


    /**
     * @User: 刘海龙
     * @Date: 2021/8/30
     * @Time: 16:49
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 获取配置
     */
    public function getSystemConfig()
    {
        $res = SystemConfig::select()->toArray();
        $new_data = [];
        foreach ($res as $key => $v) {
            $new_data[$v['name']] = $v['value'];
        }
        if (empty($new_data)) {
            return $this->response(403, "暂未找到配置信息，请添加");
        }
        return $this->response($new_data);
    }


    /**
     * @User: 刘海龙
     * @Date: 2021/8/30
     * @Time: 16:47
     * @return \think\response\Json
     * @throws \Exception
     * 保存配置
     */
    public function saveSystemConfig()
    {
        $data = $this->request->only(['day_type', 'day', 'about_content', 'title', 'sensitive','template','default_head','default_bg','randomMobile','mobile','notice_type','notice']);
        $arr_key = array_keys($data);
        $new_data = [];
        foreach ($arr_key as $key => $value) {
            $new_data[] = [
                'name' => $value,
                'value' => $data[$value]
            ];
        }
        $config = new SystemConfig();
        Db::name('system_config')->delete(true);
        $config->saveAll($new_data);
        return $this->response();
    }

    //getMediaAllList
    public function getMediaAllList()
    {
        $newArr = array();
        $limit = $this->request->param('limit', 10);
        $res = Demands::paginate($limit);
        $page = $res->all();
        foreach ($page as $key => $v) {
            $newArr[$v['demand_type']]['goodsList'][] = $v;
        }
        return $this->response(array_values($newArr));

    }


    public function test()
    {
        $id = $this->request->param('id', 1);
        $res = Facts::with(['unit', 'comment' => ['reply']])->find($id);
        if (is_null($res)) {
            return $this->response(403, "暂未找到该信息");
        }
        if ($res['way_id'] !== 0) {
            $res['way_name'] = Facts::$fact_way[$res['way_id']];
        }
        $res['name'] = '刘海龙'; //此为头像
        $res['url'] = 'https://cdn.uviewui.com/uview/template/SmilingDog.jpg'; //此为头像
        $res['allReply'] = 1; //回复总数
        $res['fact_type_name'] = Facts::$fact_type[$res['fact_type']];
        $data = $res['comment'];
        foreach ($data as $key => $v) {
            $v['name']='刘海龙';
            $v['url'] = 'https://cdn.uviewui.com/uview/template/SmilingDog.jpg'; //此为头像
            $reply = $v['reply'];
            $reply['name']='张三';
            $reply['url'] = 'https://cdn.uviewui.com/uview/template/SmilingDog.jpg'; //此为头像

        }
        return $this->response($res);

    }


    /**
     * @User: 刘海龙
     * @Date: 2021/9/2
     * @Time: 19:30
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 获取关于我们
     */
    public function getAboutMeInfo(){
        $res = SystemConfig::where(['name'=>'about_content'])->find()->toArray();
        return $this->response($res);
    }


    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/9/13
     * @Time: 16:52
     * 下载模板文件
     */
    public function downloadFile($id)
    {
        if (intval($id) === 1){
            $file = public_path() . 'storage/template/online.xlsx';
        }else{
            $file = public_path() . 'storage/template/unit.xlsx';
        }
        $data = $this->downloadExcel($file);
        return $data;
    }
}
