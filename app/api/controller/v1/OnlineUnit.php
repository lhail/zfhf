<?php
declare (strict_types=1);

namespace app\api\controller\v1;

use app\api\helpers\v1\traits\FileUpload;
use app\api\helpers\v1\traits\SearchDataForModel;
use app\api\model\v1\EnterUnit;
use app\api\model\v1\OnlineUnitCount;
use app\api\model\v1\OnlineUnitDetail;
use app\api\model\v1\OnlineUnitMonth;
use app\api\model\v1\OnlineUnitYears;
use app\BaseController;
use think\facade\Db;
use think\Request;

class OnlineUnit extends BaseController
{

    use SearchDataForModel, FileUpload;


    /**
     * @User: 刘海龙
     * @Date: 2021/8/25
     * @Time: 16:03
     * @return \think\response\Json
     * @throws \think\db\exception\DbException
     * 数据列表
     */
    public function getOnlineUnitList()
    {
        $limit = $this->request->param('limit', 0);
        $res = $this->search(new OnlineUnitYears(), [], $limit);
        return $this->response($res);
    }



    public function unitAllList(){
        $res = EnterUnit::select();
        return $this->response($res);
    }

    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/8/25
     * @Time: 16:23
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 根据年份删除
     */
    public function onlineUnitDel($id)
    {
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            $res = OnlineUnitYears::with(['onlineUnitMonths', 'onlineUnitDetails'])->find($id);
            if (is_null($res)) {
                return $this->response(404, '未找到该数据');
            }
            $res->together(['onlineUnitMonths', 'onlineUnitDetails'])->force()->delete();
        }
        return $this->response();
    }

    /**
     * @User: 刘海龙
     * @Date: 2021/8/25
     * @Time: 16:33
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 数据保存
     */
    public function saveOnlineUnit(){
        $id = $this->request->param('id', 0);
        $data = $this->request->param();
        $data['create_id'] = $this->request->uid; //用户id
        if (intval($id) === 0) {
            OnlineUnitYears::create($data);
        } else {
            $res = OnlineUnitYears::find($id);
            if (is_null($res)) {
                return $this->response(403, '数据不存在');
            }
            $res->save($data);
        }
        return $this->response();
    }

    /**
     * @User: 刘海龙
     * @Date: 2021/8/25
     * @Time: 15:43
     * @return \think\response\Json
     * 导入数据
     */
    public function importOnlineUnitExcel()
    {
        if (!$this->request->file('file')) {
            return $this->response(501, '请选择上传文件');
        }
        $file = $this->request->file('file');
        $result = $this->saveImportExcel($file, 'import_excel');
        $is_bool = OnlineUnitYears::saveOnlineUnit($result, $this->request->uid);
        if ($is_bool === 200) {
            return $this->response();
        }else if ($is_bool === 403){
            return $this->response(403, '数据已经存在该年份,请不要重新导入');
        } else {
            return $this->response(403, '导入问卷失败');
        }
    }

    /**
     * @User: 刘海龙
     * @Date: 2021/8/25
     * @Time: 16:59
     * @return \think\response\Json
     * @throws \think\db\exception\DbException
     * 根据年份id获取月份
     */
    public function getOnlineUnitMonthList(){
        $limit = $this->request->param('limit', 0);
        $map[] = array('online_unit_years_id','=',$this->request->param('online_unit_years_id'));
        $res = $this->search(new OnlineUnitMonth(), $map, $limit,'asc',['details']);
        return $this->response($res);
    }

    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/8/25
     * @Time: 17:30
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 根据月份删除
     */
    public function onlineUnitMonthDel($id){
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            $res = OnlineUnitMonth::with(['details'])->find($id);
            if (is_null($res)) {
                return $this->response(404, '未找到该数据');
            }
            $res->together(['details'])->force()->delete();
        }
        return $this->response();
    }

    /**
     * @param $id
     * @User: 刘海龙
     * @Date: 2021/8/25
     * @Time: 17:36
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 删除详细指标
     */
    public function onlineUnitDetailDel($id){
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            $res = OnlineUnitDetail::find($id);
            if (is_null($res)) {
                return $this->response(404, '未找到该数据');
            }
            $res->force()->delete();
        }
        return $this->response();
    }

    /**
     * @User: 刘海龙
     * @Date: 2021/8/31
     * @Time: 9:36
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 根据当前日期获取最新的上线一周
     */
    public function getWeekOnlineUnit()
    {
        $res = Db::name('online_unit_detail')->field('id,detail_title,online_date,online_week,online_status')->whereWeek('online_date')->paginate(7)->each(function ($item, $key) {
            $item['online_status_name'] = OnlineUnitDetail::$online_status[$item['online_status']];
            return $item;
        });
        return $this->response($res);
    }
    /**
     * @User: 刘海龙
     * @Date: 2021/8/31
     * @Time: 11:46
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 获取今明2天上线的单位
     */
    public function todayOnlineUnit(){
        $today = Db::name('online_unit_detail')->field('detail_title,online_date,online_week,online_status')->whereDay('online_date')->find();
        $tomorrow = Db::name('online_unit_detail')->field('detail_title,online_date,online_week,online_status')->whereDay('online_date', 'tomorrow')->find();
        if ($today['online_status'] === OnlineUnitDetail::ONLINE_STATUS_ONE) {
            $today_msg = "今天上线的单位是:" . $today['detail_title'] . ',' . $today['online_week'] . ',' . $today['online_date'];
        } else if ($today['online_status'] === OnlineUnitDetail::ONLINE_STATUS_TWO) {
            $today_msg = "今天为:" . $today['detail_title']. ',' . $today['online_week'] . ',' . $today['online_date'];
        } else {
            $today_msg = "今天暂无上线单位". ',' . $today['online_week'] . ',' . $today['online_date'];;
        }
        if ($tomorrow['online_status'] === OnlineUnitDetail::ONLINE_STATUS_ONE) {
            $tomorrow_msg = "明天上线的单位是:" . $tomorrow['detail_title'] . ',' . $tomorrow['online_week'] . ',' . $tomorrow['online_date'];
        } else if ($tomorrow['online_status'] === OnlineUnitDetail::ONLINE_STATUS_TWO) {
            $tomorrow_msg = "明天为:" . $tomorrow['detail_title']. ',' . $tomorrow['online_week'] . ',' . $tomorrow['online_date'];;
        } else {
            $tomorrow_msg = "明天暂无上线单位". ',' . $tomorrow['online_week'] . ',' . $tomorrow['online_date'];
        }
        $new_arr = [
            $today_msg,
            $tomorrow_msg
        ];

        return $this->response($new_arr);
    }

    /**
     * @User: 刘海龙
     * @Date: 2021/9/9
     * @Time: 10:22
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 显示当天的
     */
    public function todayOnline(){
        $today = OnlineUnitDetail::with(['Rcount'])->field('id,detail_title,online_date,online_week,online_status')->whereDay('online_date')->find();
        return $this->response($today->toArray());
    }

    /**
     * @User: 刘海龙
     * @Date: 2021/9/9
     * @Time: 13:05
     * @return \think\response\Json
     * 保存一把手上线统计
     */
    public function saveOnlineCount(){
        $data = $this->request->param();
        if ($data['value'] === '否'){
            $data['is_online'] = 1;
        }else{
            $data['is_online'] = 2;
        }
        $data['user_id'] = $this->request->wx_user_id ?? 0;
        OnlineUnitCount::create($data);
        return $this->response();
    }

}
