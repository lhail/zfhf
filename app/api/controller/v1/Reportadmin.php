<?php
declare (strict_types=1);

namespace app\api\controller\v1;

use app\api\helpers\v1\traits\FileUpload;
use app\api\helpers\v1\traits\SearchDataForModel;
use app\api\model\v1\EnterUnit;
use app\api\model\v1\Facts;
use app\api\model\v1\Reply;
use app\api\model\v1\Users;
use app\api\validate\v1\EnterValidate;
use app\BaseController;
use think\exception\ValidateException;
use think\Request;

class Reportadmin extends BaseController
{

    public function facts(Request $request){
        $start = $request->param("start","2022-01-01","trim");
        $end = $request->param("end","2024-09-30","trim");

        $start.= " 00:00:00";
        $end.= " 23:59:59";

        $facts = Facts::whereNull("delete_time")
            ->whereBetween("create_time",[$start,$end])
            ->order("id ASC")
            ->select()->toArray();
        $users = Users::select()->toArray();
        $usersArr = array_column($users, null,"id");

        $enterUnit = EnterUnit::select()->toArray();
        $enterUnitArr = array_column($enterUnit, null,"id");

        $replys = Reply::whereNULL("delete_time")->select()->toArray();
        $replyArr = array_column($replys, null,"fact_id");

        echo '<table border="1">';
        echo '<tr>';
        echo '<td>录入方式</td>';
        echo '<td>时间</td>';
        echo '<td>姓名</td>';
        echo '<td>手机号</td>';
        echo '<td>咨询单位</td>';
        echo '<td>标题</td>';
        echo '<td>内容</td>';
        echo '<td>是否精选</td>';

        echo '<td>预计时间</td>';
        echo '<td>回复时间</td>';
        echo '<td>回复内容</td>';
        echo '</tr>';
        foreach ($facts as $key=>$vf){
            if(isset($usersArr[$vf['user_id']])){
                $user=$usersArr[$vf['user_id']];
                $user_name = $user['nickname']?$user['nickname']:'未填';
                $user_mobile = $user['mobile']?$user['mobile']:'未填';
            }else{
                $user_name=$user_mobile="--";
            }
            $unit = isset($enterUnitArr[$vf['unit_id']])?$enterUnitArr[$vf['unit_id']]['title']:"--";
            $status = $vf['status']==1?'精选':"否";

            $reply = isset($replyArr[$vf['id']])?$replyArr[$vf['id']]['filter_reply_content']:"--";


            echo '<tr/>';
            echo '<td>'.$vf['source'].'</td>';
            echo '<td>'.$vf['create_time'].'</td>';
            echo '<td>'.$user_name.'</td>';
            echo '<td>'.$user_mobile.'</td>';
            echo '<td>'.$unit.'</td>';
            echo '<td>'.$vf['title'].'</td>';
            echo '<td>'.$vf['filter_content'].'</td>';
            echo '<td>'.$status.'</td>';
            echo '<td>'.$vf['plan_date'].'</td>';
            echo '<td>'.$vf['reply_date'].'</td>';
            echo '<td>'.$reply.'</td>';


            echo '</tr>';
        }
        echo '</table>';

        die;
    }
}
