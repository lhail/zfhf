<?php


namespace app\api\helpers\v1\traits;


trait WeekDay
{
    /**
     * @param $time
     * @param $days
     * @User: 刘海龙
     * @Date: 2021/8/30
     * @Time: 16:37
     * @return false|string
     * 返回N天之后的工作日
     */
    public function getdates($time,$days){
        $num=0;
        while($num<$days) {
            $time=$time-86400; //往前算工作日
          //  $time=$time+86400; //往后算工作日
            $day=date('Ymd',$time);

            $isworkday=$this->getIsWorkingDay($day);

            if($isworkday==true){
                $num++;
            }
        }
        return date('Y-m-d',$time);
    }

    /**
     * @param $target_day
     * @User: 刘海龙
     * @Date: 2021/8/30
     * @Time: 16:37
     * @return bool
     * 过滤节假日
     */
    public function getIsWorkingDay($target_day)
    {
        if (!strtotime($target_day) || mb_strlen($target_day) !== 8) {
            return false;
        }

        // 特殊的假日 (传统节日放假)
        $lst_holiday = [
            '20210101' => '',          // 元旦
            '20210211' => '20210217',  // 春节
            '20210403' => '20210405',  // 清明节
            '20210501' => '20210505',  // 劳动节
            '20210612' => '20210614',  // 端午节
            '20210919' => '20210921',  // 中秋
            '20211001' => '20211007',  // 国庆  (由于 2020 年中秋和国庆同样 所以国庆的可以省略)
        ];
        // 特殊的工作日 (传统节日前后补班)
        $lst_working_day = [
            '20210207' => '',  // 春节
            '20210220' => '',  // 春节
            '20210425' => '',  // 劳动节
            '20210508' => '',  // 劳动节
            '20210918' => '',  // 中秋
            '20210926' => '',  //  国庆
            '20211009' => '',  //  国庆
        ];
        // 遍历数组 获取今天是否为假日
        foreach ($lst_holiday as $holiday_key => $holiday_value) {
            if ($holiday_key == $target_day) {
                return false;
            }

            if ($holiday_key > $target_day) {
                if (!empty($holiday_value) && $holiday_value <= $target_day) {
                    return false;
                }
            }
        }

        // 遍历数组 获取今天是否为工作日
        foreach ($lst_working_day as $working_key => $working_value) {
            if ($working_key == $target_day) {
                return true;
            }
            if ($working_key > $target_day) {
                if (!empty($working_value) && $working_value <= $target_day) {
                    // var_dump(2);
                    return true;
                }
            }
        }

        // 最后执行到这里 说明当前日期并非特殊节假日或需要补班的时间
        $week = date('w', strtotime($target_day));

        if (!$week || $week == 6) {
            // var_dump(3);
            return false;
        }

        // var_dump(4);
        return true;
    }

    /**
     * @param int $length
     * @User: 刘海龙
     * @Date: 2021/9/3
     * @Time: 16:51
     * @return int
     * 随机生成数字
     */
    public function generate_code($length = 6) {
        return rand(pow(10,($length-1)), pow(10,$length)-1);
    }
}
