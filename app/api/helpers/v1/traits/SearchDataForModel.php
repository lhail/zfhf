<?php


namespace app\api\helpers\v1\traits;


use think\Model;

trait SearchDataForModel
{
    private $builder = null;
    private $table = null;

    /**
     * @param Model $model 实例化
     * @param array $map 条件
     * @param int   $perPage  每页显示多少条
     * @User: 刘海龙
     * @Date: 2021/2/25
     * @Time: 14:27
     * @return \think\Paginator
     * @throws \think\db\exception\DbException
     *
     */
    public function search(Model $model, $map = [], $perPage = 100,$ordPage='desc',$with=[])
    {
        $table = $model->getTable();
        $this->builder = $model->newQuery(); //查询构造器
        $this->table = $table;
        if (method_exists($this, $table)) {
            $this->$table();
        }
        return $model->with($with)->where($map)->order($table . '.id', $ordPage)->paginate($perPage);
    }
}
