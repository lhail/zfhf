<?php

use think\migration\Migrator;
use think\migration\db\Column;

class UpdateCreateReply extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('reply', ['comment' => '回复表']);
        $table
            ->addColumn('is_attachment', 'integer', array('limit' => 30, 'default' => 1, 'comment' => '是否存在附件（默认1,2存在）'))
            ->addColumn('img_path', 'json', array('null' => true, 'comment' => '附件地址'))
            ->save();
    }
}
