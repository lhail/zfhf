<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateFactsOut extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('facts_out', ['comment' => '意见退回表']);
        $table->addColumn('fact_id', 'integer', array('limit' => 30, 'null' => true, 'comment' => '爆料id'))
            ->addColumn('operation_id', 'integer', array('limit' => 30, 'default' => 0, 'comment' => '同意或者拒绝人'))
            ->addColumn('user_id', 'integer', array('limit' => 30, 'default' => 0, 'comment' => '退回人'))
            ->addColumn('out_content', 'text', array('null' => true, 'comment' => '退回理由'))
            ->addColumn('out_status', 'integer', array('limit' => 30, 'default' => 0, 'comment' => '退回状态(0,暂未分配,1同意退回,2.拒绝退回)'))
            ->addColumn('refuse_content', 'text', array('null' => true, 'comment' => '主持人拒绝理由'))
            ->addTimestamps()
            ->addSoftDelete()
            ->create();
    }
}
