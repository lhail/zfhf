<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateUsers extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('users', ['comment' => '微信用户表']);
        $table->addColumn('nickname', 'string',array('limit' => 60, 'null' => true,'comment' => '昵称'))
            ->addColumn('mobile', 'string',array('limit' => 15,'null' => true, 'comment' => '手机号码 账号'))
            ->addColumn('email', 'string',array('limit' => 50,'null' => true,'comment' => '邮箱'))
            ->addColumn('token', 'string',array('limit' => 450,'null' => true, 'comment' => '登录token'))
            ->addColumn('head_img', 'string',array('null' => true, 'comment' => '用户头像'))
            ->addColumn('admin_id', 'integer',array('limit' => 30,'default' => 0, 'comment' => '关联的是后台的哪个人员的账号信息'))
            ->addColumn('is_management', 'boolean',array('limit' => 1, 'default' => 1, 'comment' => '是否为管理人员(1是,0游客)'))
            ->addColumn('is_auth', 'boolean',array('limit' => 1, 'default' => 1, 'comment' => '权限(1,主持人(超级管理员),2单位负责人权限,3游客)'))
            ->addColumn('unit_id', 'integer',array('limit' => 11,'default' => 0, 'comment' => '所属单位id'))
            ->addColumn('openid', 'string',array('limit' => 80,'null' => true, 'comment' => '微信openid'))
            ->addColumn('session_key', 'string',array('limit' => 80,'null' => true, 'comment' => '微信session_key'))
            ->addColumn('remark', 'string',array('limit' => 30,'null' => true, 'comment' => '用户备注'))
            ->addTimestamps()
            ->addSoftDelete()
            ->addIndex(array('mobile'), array('unique' => true))
            ->addIndex(array('email'), array('unique' => true))
            ->create();
    }
}
