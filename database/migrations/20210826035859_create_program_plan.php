<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateProgramPlan extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        //节目预案表
        $table = $this->table('program_plan', ['comment' => '节目预案表']);
        $table->addColumn('title', 'string', array('limit' => 220, 'null' => true, 'comment' => '节目主题'))
            ->addColumn('online_unit_id', 'integer', array('limit' => 30, 'default' => 0, 'comment' => '上线单位id'))
            ->addColumn('enter_unit_id', 'integer', array('limit' => 30, 'default' => 0, 'comment' => '入驻单位id'))
            ->addColumn('guest_id', 'integer', array('limit' => 30, 'default' => 0, 'comment' => '嘉宾id'))
            ->addColumn('create_id', 'integer', array('limit' => 30, 'default' => 0, 'comment' => '创建人id'))
            ->addColumn('accessory_status', 'boolean', array('limit' => 1, 'default' => 0, 'comment' => '是否上传附件(0未传,1已传)'))
            ->addColumn('accessory_path', 'string', array('limit' => 230,'null' => true, 'comment' => '预案附件'))
            ->addColumn('source', 'string', array('limit' => 30, 'null' => true, 'comment' => '来源'))
            ->addColumn('content', 'text', array('null' => true, 'comment' => '预案内容'))
            ->addTimestamps()
            ->addSoftDelete()
            ->addIndex(array('title'))
            ->create();

//        //该节目是否上传预案记录表
//        $table = $this->table('program_plan_unit_access', ['comment' => '节目预案明细表(是否提交预案)']);
//        $table->addColumn('program_plan_id', 'integer', array('limit' => 30, 'default' => 0, 'comment' => '预案id'))
//            ->addColumn('on_unit_id', 'integer', array('limit' => 30, 'default' => 0, 'comment' => '上线单位id'))
//            ->addColumn('en_unit_id', 'integer', array('limit' => 30, 'default' => 0, 'comment' => '入驻单位id'))
//            ->addTimestamps()
//            ->addSoftDelete()
//            ->create();
    }
}
