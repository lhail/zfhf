<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateOnlineUnitCount extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('online_unit_count', ['comment' => '上线率统计']);
        $table
            ->addColumn('replace_before_unit_id', 'integer',array('limit' => 30, 'default' => 0, 'comment' => '替换前的上线单位单位id(对应online_unit_detail)'))
            ->addColumn('replace_before_unit_name', 'string',array('limit' => 130, 'default' => 0, 'comment' => '替换前的上线单位名称(对应online_unit_detail)'))
            ->addColumn('replace_after_unit_id', 'integer',array('limit' => 30, 'default' => 0, 'comment' => '替换后的入驻单位id（对应enter_unit）'))
            ->addColumn('replace_after_unit_name', 'string',array('limit' => 130, 'default' => 0, 'comment' => '替换后的入驻单位名字（对应enter_unit）'))
            ->addColumn('is_online', 'boolean',array('limit' => 1, 'default' => 1, 'comment' => '一把手是否上线(1否 2是)'))
            ->addColumn('source', 'string', array('limit' => 30, 'null' => true, 'comment' => '来自哪个终端操作PC or wx'))
            ->addColumn('status_type', 'boolean',array('limit' => 1, 'default' => 1, 'comment' => '类别(1正常 2转入)'))
            ->addColumn('user_id', 'integer', array('limit' => 30, 'default' => 0, 'comment' => '操作人'))
            ->addTimestamps()
            ->addSoftDelete()
            ->create();
    }
}
