<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateAdmins extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('admins', ['comment' => '管理员表']);
        $table->addColumn('username', 'string',array('limit' => 20, 'comment' => '用户名'))
            ->addColumn('mobile', 'string',array('limit' => 15, 'comment' => '手机号码 账号'))
            ->addColumn('email', 'string',array('limit' => 50, 'comment' => '邮箱'))
            ->addColumn('password', 'string',array('limit' => 150, 'comment' => '登录密码'))
            ->addColumn('token', 'string',array('limit' => 350,'default' => '', 'comment' => '登录token'))
            ->addColumn('head_img', 'string',array('default' => '', 'comment' => '用户头像'))
            ->addColumn('login_status', 'boolean',array('limit' => 1, 'default' => 0, 'comment' => '登陆状态'))
            ->addColumn('login_code', 'string',array('limit' => 32, 'default' => 0, 'comment' => '排他性登陆标识'))
            ->addColumn('last_login_ip', 'integer',array('limit' => 11, 'default' => 0, 'comment' => '最后登录IP'))
            ->addColumn('last_login_time', 'datetime',array('comment' => '最后登录时间'))
            ->addColumn('remark', 'string',array('limit' => 30, 'default' => '', 'comment' => '用户备注'))
            ->addColumn('create_id', 'integer',array('limit' => 30, 'comment' => '创建人id'))
            ->addTimestamps()
            ->addSoftDelete()
            ->addIndex(array('username'))
            ->addIndex(array('mobile'), array('unique' => true))
            ->addIndex(array('email'), array('unique' => true))
            ->create();
    }
}
