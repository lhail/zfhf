<?php

use think\migration\Migrator;
use think\migration\db\Column;

class UpdateFactPlan extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {

        $table = $this->table('facts', ['comment' => '爆料表']);
        $table->addColumn('plan_date', 'date',array('null' => true,'comment' => '计划日期:7个工作日'))
            ->save();
    }
}
