<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateOnlineUnitYears extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('online_unit_years', ['comment' => '上线单位年份表']);
        $table->addColumn('years_title', 'string',array('limit' => 120, 'null' => true,'comment' => '标题'))
            ->addColumn('years', 'string',array('limit' => 120, 'null' => true,'comment' => '年份'))
            ->addColumn('create_id', 'integer',array('limit' => 30,'default' => 0,  'comment' => '创建人id'))
            ->addTimestamps()
            ->addSoftDelete()
            ->addIndex(array('years_title'))
            ->create();

        $table = $this->table('online_unit_month', ['comment' => '上线单位月份表']);
        $table->addColumn('month_title', 'string',array('limit' => 120, 'null' => true,'comment' => '标题'))
            ->addColumn('online_unit_years_id', 'integer',array('limit' => 30,'default' => 0,  'comment' => '年份id'))
            ->addColumn('month', 'string',array('limit' => 120, 'null' => true,'comment' => '月份'))
            ->addTimestamps()
            ->addSoftDelete()
            ->addIndex(array('month_title'))
            ->create();
        $table = $this->table('online_unit_detail', ['comment' => '上线单位详细表']);
        $table->addColumn('detail_title', 'string',array('limit' => 120, 'null' => true,'comment' => '标题'))
            ->addColumn('online_unit_years_id', 'integer',array('limit' => 30,'default' => 0,  'comment' => '年份id'))
            ->addColumn('online_unit_month_id', 'integer',array('limit' => 30,'default' => 0,  'comment' => '月份id'))
            ->addColumn('parent_id', 'integer',array('limit' => 11, 'default' => 0, 'comment' => '归属层级'))
            ->addColumn('online_date', 'date',array('null' => true,'comment' => '上线单位日期'))
            ->addColumn('online_week', 'string',array('limit' => 120, 'null' => true,'comment' => '星期几'))
            ->addColumn('remark', 'string',array('limit' => 130, 'null' => true, 'comment' => '备注'))
            ->addColumn('create_id', 'integer',array('limit' => 30,'default' => 0,  'comment' => '创建人id'))
            ->addColumn('online_status', 'boolean',array('limit' => 1, 'default' => 1, 'comment' => '上线状态(1上线,2回复版,3.不上线)'))
            ->addTimestamps()
            ->addSoftDelete()
            ->addIndex(array('detail_title'))
            ->create();
    }
}
