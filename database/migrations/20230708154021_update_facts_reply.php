<?php

use think\migration\Migrator;
use think\migration\db\Column;

class UpdateFactsReply extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('facts', ['comment' => '爆料表']);
        $table->addColumn('reply_date', 'datetime',array('null' => true,'comment' => '回复时间'))
            ->addColumn("plan","integer",['limit'=>1,'default'=>1,'comment'=>'计划状态1正常2逾期'])
            ->save();

    }
}
