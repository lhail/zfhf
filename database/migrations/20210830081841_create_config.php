<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateConfig extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('system_config', ['comment' => '配置表']);
        $table->addColumn('name', 'string', array('limit' => 120, 'null' => true, 'comment' => '配置名称'))
            ->addColumn('value', 'text', array('null' => true, 'comment' => '配置值'))
            ->addTimestamps()
            ->addSoftDelete()
            ->addIndex(array('name'))
            ->create();
    }
}
