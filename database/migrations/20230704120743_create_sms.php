<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateSms extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table("sms");
        $table
            ->addColumn("tel",'string',['limit'=>13,'null'=>true,'comment'=>'电话号码'])
            ->addColumn("content",'string',['limit'=>100,'null'=>true,'comment'=>'内容'])
            ->addColumn("remark",'string',['limit'=>512,'null'=>true,'comment'=>'反馈'])
            ->addColumn("fact_id",'integer',['limit'=>10,'default'=>0,'comment'=>'爆料id'])
            ->addColumn("type",'integer',['limit'=>3,'default'=>0,'comment'=>'类型'])
            ->addTimestamps()
            ->addSoftDelete()
            ->save();


    }
}
