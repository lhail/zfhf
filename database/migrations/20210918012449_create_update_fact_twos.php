<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateUpdateFactTwos extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('facts', ['comment' => '爆料表']);
        $table->addColumn('is_show', 'integer', array('limit' => 30, 'default' => 1, 'comment' => '是否显示（默认显示1,2为不显示）'))
            ->addColumn('is_close', 'integer', array('limit' => 30, 'default' => 1, 'comment' => '是否强制关闭案件（默认不关闭1,2为关闭）'))
            ->save();
    }
}
