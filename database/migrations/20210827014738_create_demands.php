<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateDemands extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('demands', ['comment' => '点播表']);
        $table->addColumn('title', 'string', array('limit' => 120, 'null' => true, 'comment' => '名称'))
            ->addColumn('desc', 'string', array('limit' => 320, 'null' => true, 'comment' => '描述'))
            ->addColumn('demand_type', 'integer', array('limit' => 30, 'default' => 0, 'comment' => '点播类型(1直播,2音频,3视频)'))
            ->addColumn('external_link', 'string', array('limit' => 130, 'null' => true, 'comment' => '外部链接'))
            ->addColumn('cover', 'string', array('limit' => 130, 'null' => true, 'comment' => '封面图'))
            ->addColumn('create_id', 'integer', array('limit' => 30, 'default' => 0, 'comment' => '创建人id'))
            ->addColumn('online_unit_id', 'integer', array('limit' => 30, 'default' => 0, 'comment' => '上线单位id'))
            ->addColumn('status', 'boolean', array('limit' => 1,'default' => 0, 'comment' => '状态(1首页置顶,2内页置顶,0默认)'))
            ->addColumn('is_accessory', 'boolean', array('limit' => 1,'default' => 0, 'comment' => '是否存在音频(0默认,1存在)'))
            ->addColumn('accessory_path', 'json', array('null' => true, 'comment' => '附件地址(可存音视频上传地址)'))
            ->addTimestamps()
            ->addSoftDelete()
            ->addIndex(array('title'))
            ->create();
    }
}
