<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateUpdateTuijian extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('program_recommendation', ['comment' => '节目推荐表']);
        $table
            ->addColumn('create_id', 'integer', array('limit' => 30, 'default' => 0, 'comment' => '创建人id'))

            ->save();
    }
}
