<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateCarousel extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('carousel', ['comment' => '轮播图表']);
        $table->addColumn('title', 'string', array('limit' => 120, 'null' => true, 'comment' => '图名称'))
            ->addColumn('carousel_type', 'integer', array('limit' => 30, 'default' => 0, 'comment' => '类型(1首页,2点播页,3爆料页)'))
            ->addColumn('link', 'string', array('limit' => 130, 'null' => true, 'comment' => '外部链接'))
            ->addColumn('create_id', 'integer', array('limit' => 30, 'default' => 0, 'comment' => '创建人id'))
            ->addColumn('order', 'integer', array('limit' => 30, 'default' => 1, 'comment' => '排序'))
            ->addColumn('path', 'string', array('limit' => 330, 'null' => true, 'comment' => '地址'))
            ->addTimestamps()
            ->addSoftDelete()
            ->addIndex(array('title'))
            ->create();
    }
}
