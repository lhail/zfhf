<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateReply extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('reply', ['comment' => '回复表']);
        $table->addColumn('fact_id', 'integer', array('limit' => 30, 'null' => true, 'comment' => '爆料id'))
            ->addColumn('comment_id', 'integer', array('limit' => 30, 'default' => 0, 'comment' => '评论id'))
            ->addColumn('reply_user_id', 'integer', array('limit' => 30, 'default' => 0, 'comment' => '回复人'))
            ->addColumn('reply_to_user_id', 'integer', array('limit' => 30, 'default' => 0, 'comment' => '回复给某个人的id'))
            ->addColumn('reply_content', 'text', array('null' => true, 'comment' => '内容(原始内容)'))
            ->addColumn('filter_reply_content', 'text', array('null' => true, 'comment' => '内容(过滤后的)'))
            ->addTimestamps()
            ->addSoftDelete()
            ->create();
    }
}
