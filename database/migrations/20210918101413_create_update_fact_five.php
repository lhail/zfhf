<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateUpdateFactFive extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('facts', ['comment' => '爆料表']);
        $table->addColumn('is_hidden', 'integer', array('limit' => 30, 'default' => 1, 'comment' => '是否隐藏手机号（默认不隐藏1,2为隐藏（隐藏手机号后，只能主持人看到））'))
            ->save();
    }
}
