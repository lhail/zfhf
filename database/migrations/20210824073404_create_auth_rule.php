<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateAuthRule extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('auth_rule', ['comment' => '规则表']);
        $table->addColumn('name', 'string',array('limit' => 100,'null' => true, 'comment' => '规则唯一标识'))
            ->addColumn('title','string',array('limit'=>20,'null' => true,'comment'=>'规则中文名称'))
            ->addColumn('status', 'boolean',array('limit' => 1, 'default' => 1, 'comment' => '状态：为1正常，为0禁用'))
            ->addColumn('condition','string',array('limit'=>100,'default' => '','null' => true, 'comment'=>'规则表达式，为空表示存在就验证，不为空表示按照条件验证'))
            ->addColumn('parent_id', 'integer',array('limit' => 11, 'default' => 0, 'comment' => '上级菜单id'))
            ->addColumn('sort', 'integer',array('limit' => 11, 'default' => 0, 'comment' => '排序序号'))
            ->addColumn('icon','string',array('limit'=>100,'default' => '','null' => true,'comment'=>'未激活图标'))
            ->addColumn('icon_activation','string',array('limit'=>100,'default' => '','null' => true,'comment'=>'未激活图标'))
            ->addColumn('type', 'boolean',array('limit' => 1, 'default' => 1, 'comment' => ' 系统功能, 2 用户功能'))
            ->addColumn('remark','string',array('default' => '','null' => true,'comment'=>'备注信息'))
            ->addColumn('is_open', 'boolean',array('limit' => 1, 'default' => 1,'null' => true, 'comment' => '是否开启'))
            ->addColumn('level', 'integer',array('limit' => 11, 'default' => 1, 'comment' => '菜单等级'))
            ->addTimestamps()
            ->addSoftDelete()
            ->addIndex(array('name'), array('unique' => true))
            ->create();
    }
}
