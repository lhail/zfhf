<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateFacts extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('facts', ['comment' => '爆料表']);
        $table->addColumn('title', 'string', array('limit' => 120, 'null' => true, 'comment' => '爆料标题'))
            ->addColumn('user_id', 'integer', array('limit' => 30, 'default' => 0, 'comment' => '爆料人'))
            ->addColumn('fact_type', 'boolean', array('limit' => 1, 'default' => 0, 'comment' => '爆料类型(1投诉,2建议,3咨询,4其他)'))
            ->addColumn('unit_id', 'integer', array('limit' => 30, 'default' => 0, 'comment' => '爆料单位id'))
            ->addColumn('way_id', 'boolean', array('limit' => 1, 'default' => 0, 'comment' => '爆料方式(1音频,2视频,3图片)'))
            ->addColumn('is_exist', 'boolean', array('limit' => 1, 'default' => 0, 'comment' => '是否存在图片或者视频或者音频（0,不存在,1存在）'))
            ->addColumn('is_finish', 'boolean', array('limit' => 1, 'default' => 0, 'comment' => '是否结束（0,未结束,1结束完成）'))
            ->addColumn('status', 'boolean', array('limit' => 1,'default' => 0, 'comment' => '状态(0默认,1.精选)'))
            ->addColumn('latitude','string',array('limit'=>120,'null' => true,'comment'=>'纬度'))
            ->addColumn('longitude','string',array('limit'=>120,'null' => true,'comment'=>'经度'))
            ->addColumn('address','string',array('limit'=>120,'null' => true,'comment'=>'地址'))
            ->addColumn('content', 'text', array('null' => true, 'comment' => '爆料内容(原始内容)'))
            ->addColumn('filter_content', 'text', array('null' => true, 'comment' => '爆料内容(过滤后的)'))
            ->addColumn('accessory_path', 'json', array('null' => true, 'comment' => '附件地址'))
            ->addTimestamps()
            ->addSoftDelete()
            ->addIndex(array('title'))
            ->create();
    }
}
