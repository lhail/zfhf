<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateFactScore extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('fact_score', ['comment' => '打分分值表']);
        $table->addColumn('fact_id', 'integer', array('limit' => 30, 'null' => true, 'comment' => '爆料id'))
            ->addColumn('score', 'integer', array('limit' => 30, 'default' => 0, 'comment' => '分值'))
            ->addColumn('user_id', 'integer', array('limit' => 30, 'default' => 0, 'comment' => '打分人'))
            ->addTimestamps()
            ->addSoftDelete()
            ->create();
    }
}
