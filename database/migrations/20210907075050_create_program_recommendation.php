<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateProgramRecommendation extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('program_recommendation', ['comment' => '节目推荐表']);
        $table->addColumn('title', 'string', array('limit' => 220, 'null' => true, 'comment' => '名称'))
            ->addColumn('cover', 'string', array('limit' => 230,'null' => true, 'comment' => '节目封面'))
            ->addColumn('link', 'string', array('limit' => 230, 'null' => true, 'comment' => '外链'))
            ->addColumn('status', 'boolean', array('limit' => 1, 'default' => 0, 'comment' => '是否关闭'))
            ->addColumn('content', 'text', array('null' => true, 'comment' => '内容'))
            ->addTimestamps()
            ->addSoftDelete()
            ->addIndex(array('title'))
            ->create();
    }
}
